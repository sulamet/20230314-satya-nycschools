//
//  HomeViewTestCases.swift
//  Satya-NYCSchoolsTests
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import XCTest
@testable import Satya_NYCSchools
import OHHTTPStubs

 class HomeViewTestCases: BaseTestCase {
     

     override func setUpWithError() throws {
         // Put setup code here. This method is called before the invocation of each test method in the class.
         try super.setUpWithError()
     }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
     
     
     func testCombineAPI() {
         
         addStub(path: schoolsAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)
         addStub(path: schoolsSatAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)
         
         let expectation = XCTestExpectation(description: "Get All Schools Data ")
         let viewModel = HomeViewModel()
         viewModel.fetchSchoolsData { error in
             XCTAssertNil(error)
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
         
     }
     
     func testCombineAPIWithSATError() {
         
         addStub(path: schoolsAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)
         addStub(path: schoolsSatAPIPath, responseJsonFile: nil, error: NSError(domain: "SAT API Fail", code: 404, userInfo: nil))
         let expectation = XCTestExpectation(description: "Get All Schools Data with SAT Error")
         let viewModel = HomeViewModel()
         viewModel.fetchSchoolsData { error in
             XCTAssert(error == APIError.SATApiError)
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
         
     }
     
     
     func testSATDetailsByDbn() {
         
         addStub(path: schoolsAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)
         addStub(path: schoolsSatAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)
         
         let schoolDbn = "01M448"
         
         let expectation = XCTestExpectation(description: "Fetch SAT details ")
         let viewModel = HomeViewModel()
         viewModel.fetchSchoolsData { error in
             XCTAssertNotNil(viewModel.getSATDataWith(uniqueid: schoolDbn))
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
     }

}
