//
//  TestConstants.swift
//  Satya-NYCSchoolsTests
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

let kAPIRequestWaitTime = TimeInterval(10)

let schoolsAPIPath = "/resource/s3k6-pzi2.json"
let schoolsAPIResponse = "schools.json"

let schoolsSatAPIPath = "/resource/f9bf-2cp4.json"
let satAPIResponse = "schoolSATResponse.json"

//struct TestConstants {
//   static let kAPIRequestWaitTime = TimeInterval(10)
//
//   static let schoolsAPIPath = "/resource/s3k6-pzi2.json"
//    static let schoolsAPIResponse = "schools.json"
//
//    static let schoolsSatAPIPath = "/resource/f9bf-2cp4.json"
//    static let satAPIResponse = "sat.json"
//}
