//
//  BaseTestCase.swift
//  Satya-NYCSchoolsTests
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import XCTest
@testable import Satya_NYCSchools
import OHHTTPStubs

 class BaseTestCase: XCTestCase {

    func addStub(path: String, responseJsonFile: String?, error: Error?) {
        stub(condition: isPath(path)) { (request) -> HTTPStubsResponse in
            if let responseJsonFile = responseJsonFile {
                let stubPath  = OHPathForFile(responseJsonFile, type(of: self))
                return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
            } else if let error = error {
                return HTTPStubsResponse(error: error)
            }
            return HTTPStubsResponse(error: NSError(domain: "Testing Error", code: 404, userInfo: nil))
        }
    }

}
