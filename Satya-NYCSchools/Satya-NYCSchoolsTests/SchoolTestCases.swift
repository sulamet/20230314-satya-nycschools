//
//  SchoolTestCases.swift
//  Satya-NYCSchoolsTests
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import XCTest
@testable import Satya_NYCSchools
import OHHTTPStubs

final class SchoolTestCases: BaseTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSchoolsAPI() {
        
        addStub(path: schoolsAPIPath, responseJsonFile: "schools.json", error: nil)

        let expectation = XCTestExpectation(description: "Get Schools Data API")
        let viewModel = HomeViewModel()
        viewModel.getSchoolsList { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: kAPIRequestWaitTime)
    }
    
    func testSchoolsAPIDataCount() {
        
        addStub(path: schoolsAPIPath, responseJsonFile: schoolsAPIResponse, error: nil)

        let expectation = XCTestExpectation(description: "Get Schools Data API")
        let viewModel = HomeViewModel()
        viewModel.getSchoolsList { error in
            XCTAssertTrue(viewModel.schoolsData.count > 0)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: kAPIRequestWaitTime)
    }
    
    func testSchoolsAPIError() {
        
        addStub(path: schoolsAPIPath, responseJsonFile: nil, error: NSError(domain: "Schools API Fail", code: 404, userInfo: nil))

        let expectation = XCTestExpectation(description: "Get Schools Data API")
        let viewModel = HomeViewModel()
        viewModel.getSchoolsList { error in
            XCTAssert(error != nil)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: kAPIRequestWaitTime)
    }
    

}
