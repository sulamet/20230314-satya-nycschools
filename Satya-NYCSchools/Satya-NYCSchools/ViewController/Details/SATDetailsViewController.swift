//
//  SATDetailsViewController.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import UIKit
import MapKit

class SATDetailsViewController: UIViewController {
    
    private enum Constants {
        static let averageReadingScoreTitle = "SAT Average Reading Score : "
        static let averageWritingScoreTitle = "SAT Average Writing Score : "
        static let averageMathsScoreTitle = "SAT Average Maths Score : "
        static let overView = "Overview : "
        static let phoneNumber = "Phone Number : "
        static let primaryAddress = "Primary Address : "
    }
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var readingSATScoreLabel: UILabel!
    @IBOutlet weak var mathSATScoreLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var primaryAddress1: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var schoolSATModel:SchoolSATDataModel!
    
    var satDataDisplayModel:SATDataDisplayModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Details"
        prepareData()
        
    }
    
    func prepareData(){
        
        guard let data = schoolSATModel else {
            return
        }
        
        schoolNameLabel.text = data.schoolName
        
        readingSATScoreLabel.text = Constants.averageReadingScoreTitle + (data.satCriticalReadingAvgScore ?? "NA")
        writingLabel.text = Constants.averageWritingScoreTitle + (data.satWritingAvgScore ?? "NA")
        mathSATScoreLabel.text = Constants.averageMathsScoreTitle + (data.satMathAvgScore ?? "NA")
        overviewLabel.text = Constants.overView + (satDataDisplayModel.schoolOverView ?? "")
        phoneNumberLabel.text = Constants.phoneNumber + (satDataDisplayModel.phoneNumber ?? "NA")
        primaryAddress1.text = Constants.primaryAddress + (satDataDisplayModel.primaryAddress1 ?? "NA")
        addAnnotation()
    }
    
    
    func addAnnotation() {
        
        guard let latitute = satDataDisplayModel.schoolLatitute, let longitute = satDataDisplayModel.schoolLongitute else {
            return
        }
        
        let annotation = MKPointAnnotation()
        annotation.title = schoolSATModel.schoolName ?? ""
        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(latitute) ?? 0.0),
                                                       longitude: CLLocationDegrees(Double(longitute) ?? 0.0))
        
        self.mapView.addAnnotation(annotation)
        mapView.centerCoordinate = annotation.coordinate;
        
    }
    
}
