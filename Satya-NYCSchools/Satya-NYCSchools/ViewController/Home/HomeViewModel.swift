//
//  HomeViewModel.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

class HomeViewModel {
    
    /// Store all schools data
    fileprivate var rawSchoolsData = [SchoolDataModel]()
    
    var schoolsData:SchoolsData {
        if searchString.isEmpty {
            return rawSchoolsData
        } else {
            let filteredSchoolsData = rawSchoolsData.filter { (school) -> Bool in
                return school.schoolName?.containsIgnoringCase(find: searchString) ?? false
            }
            return filteredSchoolsData
        }
    }
    var schoolSATData: SchoolsSATData = []
    fileprivate(set) var searchString = ""
    
    /// Fetch Schools and SAT API
    /// - Parameter completion: completion handler with Error
    func fetchSchoolsData(completion:@escaping((APIError?) -> Void)) {
        
        let group = DispatchGroup()
        var schoolListError: APIError? = nil
        var satScoresError: APIError? = nil
        group.enter()
        
        getSchoolsList { error in
            schoolListError = error
            group.leave()
        }
        group.enter()
        getSchoolSaTData { error in
            satScoresError = error
            group.leave()
        }
        
        group.notify(queue: .main)  {
            
            guard schoolListError == nil, satScoresError == nil else {
                completion(schoolListError ?? satScoresError)
                return
            }
            completion(nil)
        }
    }
    
    /// Get schools API call
    /// - Parameter completion: returns error with optional
    func getSchoolsList(completion: @escaping (_ error:APIError?) -> Void) {
        
        NetworkManager.sharedInstance.fetchSchoolsAPIrequest { schoolData, error in
            
            if error == nil {
                self.rawSchoolsData = schoolData ?? []
                completion(nil)
            } else {
                completion(APIError.schoolApiError)
            }
        }
    }
    
    /// Get SAT API call
    /// - Parameter completion:  returns error with optional
    func getSchoolSaTData(completion: @escaping (_ error:APIError?) -> Void) {
        NetworkManager.sharedInstance.fetchSchoolSATAPIrequest { satData, error in
            if error == nil {
                self.schoolSATData = satData ?? []
                completion(nil)
            } else {
                completion(APIError.SATApiError)
            }
        }
    }
    
    /// Filter SAT data from schools data
    /// - Parameter uniqueid: String value. ex: dbn
    /// - Returns: value returns after filter
    func getSATDataWith(uniqueid: String) -> SchoolSATDataModel? {
        
        return schoolSATData.first { schoolDataModel in
            schoolDataModel.dbn == uniqueid
        }
    }
    
    func searchSchool(string: String) {
        searchString = string
    }
    
}
