//
//  SchoolsDataModel.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

// MARK: - SchoolsData
struct SchoolDataModel: Codable {
    let dbn, schoolName, overviewParagraph, languageClasses: String?
    let location, phoneNumber, schoolEmail, website: String?
    let primaryAddressLine1, city, zip, stateCode: String?
    let latitude, longitude: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case languageClasses = "language_classes"
        case location
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case primaryAddressLine1 = "primary_address_line_1"
        case city, zip
        case stateCode = "state_code"
        case latitude, longitude
    }
}

typealias SchoolsData = [SchoolDataModel]

// MARK: - SchoolsScoreSAT
struct SchoolSATDataModel: Codable {
    let dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String?
    let satMathAvgScore, satWritingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

typealias SchoolsSATData = [SchoolSATDataModel]


class SATDataDisplayModel {
    var schoolLatitute: String? = nil
    var schoolLongitute: String? = nil
    var phoneNumber:String? = nil
    var schoolOverView:String? = nil
    var primaryAddress1: String? = nil
    
}
