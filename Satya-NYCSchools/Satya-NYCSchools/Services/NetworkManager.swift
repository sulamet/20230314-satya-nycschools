//
//  NetworkManager.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

class NetworkManager {
    static let sharedInstance = NetworkManager()
    
    private init() {
        
    }
  
    func fetchSchoolsAPIrequest(completion: @escaping ((SchoolsData?, Error?) -> Void)) {
        
        let urlString = Constants.kBaseUrl + Constants.gethSchools
        
        guard let url = URL(string: urlString) else {
            completion(nil, NSError(domain: "", code: -1, userInfo: [kCFErrorLocalizedDescriptionKey as String : NSLocalizedString("Error in creating schools URL.", comment:"Error in creating schools URL")]))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (responseData, response
            , error) in
        
            if error != nil {
                print("Error: Received Error.")
                completion(nil, error)
                return
            }
            
            guard let schoolData = responseData else {
                print("Error: did not receive data")
                completion(nil, error)
                return
            }
            
            Utilites.dataToObject(type: SchoolsData.self, from: schoolData, completion: completion)
        }
        task.resume()
    }
    
    
    func fetchSchoolSATAPIrequest(completion: @escaping ((SchoolsSATData?, Error?) -> Void)) {
        
        let urlString = Constants.kBaseUrl + Constants.getSATScores
        
        guard let url = URL(string: urlString) else {
            completion(nil, NSError(domain: "", code: -1, userInfo: [kCFErrorLocalizedDescriptionKey as String : NSLocalizedString("Error in creating school SAT URL.", comment:"Error in creating schoolSAT URL")]))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (responseData, response
            , error) in
        
            if error != nil {
                print("Error: Received Error.")
                completion(nil, error)
                return
            }
            
            guard let schoolData = responseData else {
                print("Error: did not receive data")
                completion(nil, error)
                return
            }
            
            Utilites.dataToObject(type: SchoolsSATData.self, from: schoolData, completion: completion)
        }
        task.resume()
    }
    
}
