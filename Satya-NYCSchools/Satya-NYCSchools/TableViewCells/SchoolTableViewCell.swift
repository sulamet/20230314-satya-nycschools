//
//  SchoolTableViewCell.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

        
        static let identifier = "SchoolTableViewCell"
        
        @IBOutlet weak var schoolNameLabel: UILabel!
        @IBOutlet weak var cityLabel: UILabel!
        @IBOutlet weak var navigateButton: UIButton!

        var school: SchoolDataModel! {
            didSet {
                schoolNameLabel.text = school.schoolName
                if let city = school.city, let code = school.stateCode, let zip = school.zip{
                    cityLabel.text = "\(city), \(code), \(zip)"
                }
            }
        }

}
