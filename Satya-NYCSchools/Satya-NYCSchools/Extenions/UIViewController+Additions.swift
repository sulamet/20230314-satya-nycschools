//
//  UIViewController+Additions.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation
import UIKit
import MBProgressHUD


extension UIViewController {
    
    
    /// Method user for displying alert in Any View contoller
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    func showMessageWith(title:String, message:String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
