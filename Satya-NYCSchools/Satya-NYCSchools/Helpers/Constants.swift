//
//  Constants.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

struct Constants {
    
    static let kBaseUrl = "https://data.cityofnewyork.us/resource/"
    
    static let gethSchools = "s3k6-pzi2.json"
    static let getSATScores = "f9bf-2cp4.json"
    
    
}

enum APIError: Error,Equatable {
   
    case schoolApiError
    case SATApiError
    case unexpected(code: Int)
    
    public var description: String {
        switch self {
        case .schoolApiError:
            return NSLocalizedString(
                "Error while fetching schools API",
                comment: "Schools API Error"
            )
        case .SATApiError:
            return NSLocalizedString(
                "Error while fetching SAT API",
                comment: "SAT API Error"
            )
        case .unexpected(_):
            return NSLocalizedString(
                "An unexpected error occurred.",
                comment: "Unexpected Error"
            )
        }
    }
}
