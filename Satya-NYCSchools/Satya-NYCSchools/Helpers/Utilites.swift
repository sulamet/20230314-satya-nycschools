//
//  Utilites.swift
//  _14032023-Satya-NYCSchools
//
//  Created by Satyanarayana Mandala on 14/03/23.
//

import Foundation

struct Utilites {
    
    
    static func dataToObject<T>(type: T.Type, from data: Data, completion:@escaping((T?, Error?) -> Void)) where T : Decodable {
        do{
            let dataModel = try JSONDecoder().decode(type, from: data)
            completion(dataModel,nil)
        }
        catch DecodingError.dataCorrupted(let context){
            completion(nil, context.underlyingError)
        }
        catch let err {
            completion(nil, err)
        }
    }
    
}
